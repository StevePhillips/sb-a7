package ca.sb.a7;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.util.Log;
import ca.sb.a7.gps.GPSTracker;
import ca.sb.a7.R;  //.gpsmap.R;
import ca.sb.a7.models.GpsLocation;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
public class MainActivity extends Activity
{
  public static final int SHOW_LOCATIONS_DISABLED=0;
  public static final int SHOW_LOCATIONS_ENABLED=1;
  public static final int YES = 1;
  public static final int NO = 0;
  public static final int REQUEST_DESCRIPTION = 0;
  public static final int REQUEST_CONTENT = 1;
  public static Context appContext;
  List<GpsLocation> listOfGpsLocations = null;
  public static String description;
  public static String content;
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    appContext = this;
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }
  // MENU
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.generateData:
      Toast.makeText(appContext, "Generate Random Locations ",
                     Toast.LENGTH_SHORT).show();
      generateRandomLocations();
      return true;
    case R.id.deleteData:
      Toast.makeText(appContext, "Delete All Locations ", Toast.LENGTH_SHORT)
          .show();
      new Delete().from(GpsLocation.class).execute();
      return true;
    case R.id.allLocations:
      Toast.makeText(appContext, "Show All Locations", Toast.LENGTH_SHORT)
          .show();
      showAllGpsLocations();
      return true;
    case R.id.newLocation:
      Toast.makeText(appContext, "Create New GPS Location  ",
                     Toast.LENGTH_SHORT).show();
      startActivity(new Intent(this, NewGPSLocationActivity.class));
      return true;
    }
    return false;
  }

  public void clickButtonGoToMap(View view)
  {
    double latitude, longitude;
    GPSTracker gpsTracker = new GPSTracker(appContext);
    gpsTracker.getLocation();

    latitude = gpsTracker.getLatitude();
    longitude = gpsTracker.getLongitude();
    startActivity(new Intent(this, ShowMapActivity.class));
  }
  public void clickButtonCreateNewLocation(View view)
  {
    startActivity(new Intent(this, NewGPSLocationActivity.class));
  }
  public void clickButtonAllLocations(View view)
  {
    showAllGpsLocations();
  }
  public void clickButtonAllDeselectedLocations(View view)
  {
	  ArrayList<Integer> listOfGpsLocationKeys = new ArrayList<Integer>();
	  List<GpsLocation> list = new Select().from(GpsLocation.class)
			  .where("Selected =  ?", MainActivity.NO).execute();
	
	  for (int pos = 0; pos < list.size(); pos++)
	  {
		  GpsLocation gpsLocation = list.get(pos);
		  listOfGpsLocationKeys.add(gpsLocation.getId().intValue());
	  }
	  Bundle bundleShow = new Bundle();
	  bundleShow.putIntegerArrayList("gpsLocationIds", listOfGpsLocationKeys);
	  // Say that we are going to this activity to show only disabled locations
	  // and that we should be able enable them again.
	  bundleShow.putInt("intention", SHOW_LOCATIONS_DISABLED);
	  Intent showGpsLocationIntent = new Intent(this,
			  ShowGpsLocationsActivity.class);
	  showGpsLocationIntent.putExtras(bundleShow);
	  startActivity(showGpsLocationIntent);
  }
  public void clickButtonAllSelectedLocations(View view)
  {
    ArrayList<Integer> listOfGpsLocationKeys = new ArrayList<Integer>();
    List<GpsLocation> list = null;

    list = new Select().from(GpsLocation.class).where("Selected =  ?", MainActivity.YES).execute();

    for (int pos = 0; pos < list.size(); pos++)
    {
      GpsLocation gpsLocation = list.get(pos);
      listOfGpsLocationKeys.add(gpsLocation.getId().intValue());
    }
    Bundle bundleShow = new Bundle();
    bundleShow.putIntegerArrayList("gpsLocationIds", listOfGpsLocationKeys);
    bundleShow.putInt("intention", SHOW_LOCATIONS_ENABLED);
    Intent showGpsLocationIntent = new Intent(this,
        ShowGpsLocationsActivity.class);
    showGpsLocationIntent.putExtras(bundleShow);
    startActivity(showGpsLocationIntent);
  }
  public void clickButtonLocationsByTime(View view)
  {
    showAllGpsLocations();
  }
  public void clickButtonLocationsByDescription(View view)
  {
    /* It does not work well since all Dialogs are asynchronous in Android !!!!!!
    /////////////////////////////////////////////////////////////////
    AlertDialog.Builder alert = new AlertDialog.Builder(this);

    alert.setTitle("Enter a Description Substring");
    alert.setMessage("Description");

    // Set an EditText view to get user input 
    //final EditText input = new EditText(this);
    final EditText input = new EditText(this);
    alert.setView(input);

    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface dialog, int whichButton)
      {
        String value = input.getText().toString();
        MainActivity.description = value;
      }
    });

    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface dialog, int whichButton)
      {
        // Canceled.
      }
    });

    alert.show();
    /////////////////////////////////////////////////////////////////
     */
    Intent intentInputDataDialog = new Intent(this,
        InputDataDialogActivity.class);
    startActivityForResult(intentInputDataDialog, REQUEST_DESCRIPTION);
    //    ArrayList<Integer> listOfGpsLocationKeys = new ArrayList<Integer>();
    //    String strInput = "%" + MainActivity.description + "%";
    //    System.out.println("DESCRIPTION = " + strInput);
    //    List<GpsLocation> list = new Select().from(GpsLocation.class)
    //        .where("Description LIKE ?", strInput).execute();
    //    for (int pos = 0; pos < list.size(); pos++)
    //    {
    //      GpsLocation gpsLocation = list.get(pos);
    //      gpsLocation.selected = MainActivity.YES;
    //      gpsLocation.save();
    //      listOfGpsLocationKeys.add(gpsLocation.getId().intValue());
    //    }
    //
    //    Bundle bundle = new Bundle();
    //    bundle.putIntegerArrayList("gpsLocationIds", listOfGpsLocationKeys);
    //    Intent showGpsLocationIntent = new Intent(this,
    //        ShowGpsLocationsActivity.class);
    //    showGpsLocationIntent.putExtras(bundle);
    //    startActivity(showGpsLocationIntent);
  }
  public void clickButtonLocationsByContent(View view)
  {
    Intent intentInputDataDialog = new Intent(this,
        InputDataDialogActivity.class);
    startActivityForResult(intentInputDataDialog, REQUEST_CONTENT);
  }
  public void clickButtonSelectAll(View view)
  {
    List<GpsLocation> list = GpsLocation.getAll();
    for (int pos = 0; pos < list.size(); pos++)
    {
      GpsLocation gpsLocation = list.get(pos);
      gpsLocation.selected = MainActivity.YES;
      gpsLocation.save();
    }
  }
  public void clickButtonDeselectAll(View view)
  {
    List<GpsLocation> list = GpsLocation.getAll();
    for (int pos = 0; pos < list.size(); pos++)
    {
      GpsLocation gpsLocation = list.get(pos);
      gpsLocation.selected = MainActivity.NO;
      gpsLocation.save();
    }
  }
  public void showAllGpsLocations()
  {
    Bundle bundle = new Bundle();
    ArrayList<Integer> listOfIds = new ArrayList<Integer>();
    listOfGpsLocations = GpsLocation.getAll();
    for (int pos = 0; pos < listOfGpsLocations.size(); pos++)
      listOfIds.add(listOfGpsLocations.get(pos).getId().intValue());
    bundle.putIntegerArrayList("gpsLocationIds", listOfIds);
    Intent bookIntent = new Intent(this, ShowGpsLocationsActivity.class);
    bookIntent.putExtras(bundle);
    startActivity(bookIntent);
  }
  public void generateRandomLocations()
  {
    // Generate 20 random locations around Courtenay|Victoria
    final int TOTAL = 6;
    final int DIVISOR = 10;
    double latitude = 49.7512345;
    double longitude = -124.9512345;
    String description = "Description";
    String content = "Content";
    Random random = new Random();
    long timestamp;
    Calendar rightNow = Calendar.getInstance();
    timestamp = rightNow.getTimeInMillis() - 100000;
    for (int pos = 1; pos <= TOTAL; pos++)
    {
      GpsLocation gpsLocation = new GpsLocation();
      gpsLocation.description = description + pos;
      gpsLocation.content = content + pos;
      gpsLocation.timestamp = timestamp + pos * 1000;
      gpsLocation.latitude = latitude - random.nextDouble() / DIVISOR;
      ;
      gpsLocation.longitude = longitude - random.nextDouble() / DIVISOR;
      ;
      gpsLocation.selected = pos % 2 == 0 ? 1 : 0;
      gpsLocation.save();
    }
  }
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    //super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK)
    {
      Bundle bundle = data.getExtras();
      if (requestCode == REQUEST_DESCRIPTION || requestCode == REQUEST_CONTENT)
      {
        //Fetch data as above thru Intent(data)
        MainActivity.description = bundle.getString("description");
        MainActivity.content = bundle.getString("content");
        System.out.println("DESCRIPTION : " + MainActivity.description);
        System.out.println("CONTENT : " + MainActivity.content);
        Log.e("DESCRIPTION : ", MainActivity.description);
        Log.e("CONTENT : ", MainActivity.content);
        ArrayList<Integer> listOfGpsLocationKeys = new ArrayList<Integer>();
        String strInput = "%%";
        List<GpsLocation> list = null;
        if (requestCode == REQUEST_DESCRIPTION)
        {
          strInput = "%" + MainActivity.description + "%";
          list = new Select().from(GpsLocation.class).where("Description LIKE ?", strInput).execute();
        }
        else
        {
          strInput = "%" + MainActivity.content + "%";
          list = new Select().from(GpsLocation.class).where("Content LIKE ?", strInput).execute();
        }
        for (int pos = 0; pos < list.size(); pos++)
        {
          GpsLocation gpsLocation = list.get(pos);
          gpsLocation.selected = MainActivity.YES;
          gpsLocation.save();
          listOfGpsLocationKeys.add(gpsLocation.getId().intValue());
        }
        Bundle bundleShow = new Bundle();
        bundleShow.putIntegerArrayList("gpsLocationIds", listOfGpsLocationKeys);
        Intent showGpsLocationIntent = new Intent(this,
            ShowGpsLocationsActivity.class);
        showGpsLocationIntent.putExtras(bundleShow);
        startActivity(showGpsLocationIntent);
      }
    }
  }
}