package ca.sb.a7;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.activeandroid.query.Select;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import ca.sb.a7.gps.GPSTracker;
import ca.sb.a7.R;
import ca.sb.a7.MainActivity;
import ca.sb.a7.models.GpsLocation;
import android.os.Bundle;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.widget.Toast;
// -----------------------------------
public class ShowMapActivity extends FragmentActivity implements
    GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapLongClickListener,
    GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener
//GoogleMap.OnMarkerDragListener, 
//GoogleMap.InfoWindowAdapter 
{
  GoogleMap googleMap;
  List<GpsLocation> listOfGpsLocations;
  HashMap<String, Marker> mapOfMarkers;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_show_map);
    googleMap = ((SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map)).getMap();
    displayAllMarkers();
    googleMap.setOnInfoWindowClickListener(this);
    googleMap.setOnMapLongClickListener(this);
    googleMap.setOnMapClickListener(this);
    googleMap.setOnMarkerClickListener(this);
    //googleMap.setInfoWindowAdapter(this);
    //googleMap.setOnMarkerDragListener(this);
  }
  // display all markers
  public void displayAllMarkers()
  {
    mapOfMarkers = new HashMap<String, Marker>();
    listOfGpsLocations = new Select().all().from(GpsLocation.class).execute();
    //listOfGpsLocations = new Select().from(GpsLocation.class).where("Selected = ?", true).execute();

    if (listOfGpsLocations.size() == 0)
    //if ( 1 == 1)
    {
      // no markers selected, show current location
      GPSTracker gps = new GPSTracker(this);
      double latitude = 48.4328;
      double longitude = -123.3347;

      // check if GPS is enabled       
      if (gps.canGetLocation())
      {
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();
      }
      else
      {
        // Ask user to enable GPS/network in settings
        gps.showSettingsAlert();
      }

      // Creating a LatLng object for the current location
      LatLng latLng = new LatLng(latitude, longitude);

      // Showing the current location in Google Map
      googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

      // Zoom in the Google Map
      googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));

      Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng)
          .title("Here You Are").snippet("My Snippet"));
    }
    else
    {
      // diplay all Markers 
      for (int pos = 0; pos < listOfGpsLocations.size(); pos++)
      {
        GpsLocation gpsLocation = listOfGpsLocations.get(pos);


        if (gpsLocation.selected == MainActivity.YES)
        {
          int markerKey = gpsLocation.description.hashCode();

          LatLng latLng = new LatLng(gpsLocation.latitude,
              gpsLocation.longitude);

          // Showing the current location in Google Map
          googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

          // Zoom in the Google Map
          googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));


          Marker marker = googleMap.addMarker(new MarkerOptions()
              .position(latLng).title(gpsLocation.description)
              .snippet("Generated"));

          mapOfMarkers.put(gpsLocation.description, marker);
        }
      }
    }
  }

  public void calculateMultipleMarkersArea()
  {
    //      To do this, first calculate the bounds of all the markers like so:
    //
    //      LatLngBounds.Builder builder = new LatLngBounds.Builder();
    //      for each (Marker m : markers) {
    //          builder.include(m.getPosition());
    //      }
    //      LatLngBounds bounds = builder.build();
    //
    //      Then obtain a movement description object by using the factory: CameraUpdateFactory:
    //
    //      int padding = 0; // offset from edges of the map in pixels
    //      CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
    //
    //      Finally move the map:
    //
    //      googleMap.moveCamera(cu);
    //
    //      Or if you want an animation:
    //
    //      googleMap.animateCamera(cu);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.show_map, menu);
    return true;
  }


  ////////////////
  // MAP LISTENERS
  ////////////////

  @Override
  public void onInfoWindowClick(Marker marker)
  {
    Toast.makeText(this, "onInfoWindow was called", Toast.LENGTH_SHORT).show();
    // TODO Auto-generated method stub
    String title = marker.getTitle();
    GpsLocation gpsLocation = new Select().from(GpsLocation.class).where("Description = ?", title).executeSingle();
    ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
    listOfKeys.add(gpsLocation.getId().intValue());
    Bundle bundle = new Bundle();
    bundle.putIntegerArrayList("gpsLocationIds", listOfKeys);
    Intent intentShowGpsLocations = new Intent(this, ShowGpsLocationsActivity.class);
    intentShowGpsLocations.putExtras(bundle);
    startActivity(intentShowGpsLocations);

  }

  // ------------------------------------------------
  // We will use this version of map clicking to add
  // a new latitude/longitude marker thing
  // ------------------------------------
  @Override
  public void onMapLongClick(LatLng arg0)
  {
    //Toast.makeText(this, "onMapLongClick was called", Toast.LENGTH_SHORT)
    //    .show();
    // TODO Auto-generated method stub
	  Bundle bndl = new Bundle();
	  bndl.putDouble("latitude", arg0.latitude);
	  bndl.putDouble("longitude", arg0.longitude);
	  Intent intnt = new Intent(this, NewGPSLocationActivity.class);
	  intnt.putExtras(bndl);
	  // --------------------------
	  startActivity(intnt);
  }

  @Override
  public boolean onMarkerClick(Marker arg0)
  {
    Toast.makeText(this, " onMarkerClick was called  ", Toast.LENGTH_SHORT)
        .show();
    // TODO Auto-generated method stub
    return false;
  }


  @Override
  public void onMapClick(LatLng arg0)
  {
    Toast.makeText(this, " onMapClick was called  ", Toast.LENGTH_SHORT).show();
    // TODO Auto-generated method stub

  }


  //  @Override
  //  public View getInfoWindow(Marker marker)
  //  {
  //    Toast.makeText(this, "getInfoWindow was called", Toast.LENGTH_SHORT).show();
  //    // TODO Auto-generated method stub
  //    return null;
  //  }
  //  
  //  @Override
  //  public View getInfoContents(Marker marker)
  //  {
  //    Toast.makeText(this, "getInfoContents was called", Toast.LENGTH_SHORT).show();
  //    // TODO Auto-generated method stub
  //    return null;
  //  }


  //  @Override
  //  public void onMarkerDrag(Marker marker)
  //  {
  //    // TODO Auto-generated method stub
  //    
  //  }
  //
  //
  //  @Override
  //  public void onMarkerDragEnd(Marker marker)
  //  {
  //    // TODO Auto-generated method stub
  //    
  //  }
  //
  //
  //  @Override
  //  public void onMarkerDragStart(Marker marker)
  //  {
  //    // TODO Auto-generated method stub
  //    
  //  }


}
