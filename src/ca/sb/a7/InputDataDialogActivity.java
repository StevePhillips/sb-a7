package ca.sb.a7;

import ca.sb.a7.R;
import ca.sb.a7.R.layout;
import ca.sb.a7.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;


public class InputDataDialogActivity extends Activity
{

  EditText editTextDescriptionSearch;
  EditText editTextContentSearch;
  
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_input_data_dialog);
    
    editTextDescriptionSearch = (EditText)findViewById(R.id.editTextDescriptionSearch);
    editTextContentSearch = (EditText)findViewById(R.id.editTextContentSearch);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.input_data_dialog, menu);
    return true;
  }
  
  public void clickButtonDescriptionSearch(View view)
  {
    String description = editTextDescriptionSearch.getText().toString();
    Bundle bundle = new Bundle();
    bundle.putString("description", description);
    Intent intent = new Intent();
    intent.putExtras(bundle);
    setResult(RESULT_OK, intent);
    finish();
  }
  
  public void clickButtonContentSearch(View view)
  {
    String content = editTextContentSearch.getText().toString();
    
    Bundle bundle = new Bundle();
    bundle.putString("content", content);
    Intent intent = new Intent();
    intent.putExtras(bundle);
    setResult(RESULT_OK, intent);
    finish();
    
  }
  

}
