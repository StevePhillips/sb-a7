package ca.sb.a7;

import java.util.ArrayList;
import java.util.List;

import com.activeandroid.query.Select;

import ca.sb.a7.R;
import ca.sb.a7.R.layout;
import ca.sb.a7.R.menu;
import ca.sb.a7.adapters.GpsLocationAdapter;
import ca.sb.a7.models.GpsLocation;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ListView;
public class ShowGpsLocationsActivity extends Activity
{
  
  static List<GpsLocation> gpsLocationList = new ArrayList<GpsLocation>();
  ArrayList<Integer> listOfGpsLocationKeys;
  
  ListView listViewMain;
  GpsLocationAdapter gpsLocationAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    Bundle bundle = getIntent().getExtras();
    listOfGpsLocationKeys = bundle.getIntegerArrayList("gpsLocationIds");
    gpsLocationList.clear();
    
    for(int pos = 0; pos < listOfGpsLocationKeys.size(); pos++)
    {
      gpsLocationList.add((GpsLocation) new Select() .from(GpsLocation.class) .where("Id = ?", listOfGpsLocationKeys.get(pos) ) .executeSingle());
    }
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_show_gps_locations);
    setTitle("ShowGPSLocations");
    listViewMain = (ListView) findViewById(R.id.listGpsLocations);
    gpsLocationAdapter = new GpsLocationAdapter(this, gpsLocationList);
    listViewMain.setAdapter(gpsLocationAdapter);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.show_gps_locations, menu);
    return true;
  }
  
  @Override
  public void onResume()
  {
    super.onResume();
    gpsLocationList.clear();
    for(int pos = 0; pos < listOfGpsLocationKeys.size(); pos++)
    {
      gpsLocationList.add((GpsLocation) new Select() .from(GpsLocation.class) .where("Id = ?", listOfGpsLocationKeys.get(pos) ) .executeSingle());
    }
    gpsLocationAdapter = new GpsLocationAdapter(this, gpsLocationList);
    listViewMain.setAdapter(gpsLocationAdapter);
  }

}
