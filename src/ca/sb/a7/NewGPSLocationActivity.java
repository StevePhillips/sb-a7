package ca.sb.a7;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import ca.sb.a7.gps.GPSTracker;
import ca.sb.a7.R;
import ca.sb.a7.MainActivity; //bc.nic.fniscak.gpsmap.activities.MainActivity;
import ca.sb.a7.models.GpsLocation; //bc.nic.fniscak.gpsmap.models.GpsLocation;
import ca.sb.a7.models.Picture;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
public class NewGPSLocationActivity extends Activity
{
  double latitude;
  double longitude;
  long timestamp;

  TextView textViewNewLatitude;
  TextView textViewNewLongitude;
  TextView textViewNewTimestamp;
  EditText editTextNewDescription;
  EditText editTextNewContent;
  Button buttonSaveNewGpsLocation;
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_gpslocation);
    
    Bundle bndl = this.getIntent().getExtras();
    double lat = bndl.getDouble("latitude");
    double lon = bndl.getDouble("longitude");

    GPSTracker gps = new GPSTracker(this);
    // ------------------------------------
    if(lat != 0.0 && lon != 0.0)
    {
    	// ...
    	latitude = lat;
    	longitude = lon;
    }
    // check if GPS enabled       
    else if (gps.canGetLocation())
    {
      latitude = gps.getLatitude();
      longitude = gps.getLongitude();
    }
    else
    {
      // can't get location
      // GPS or Network is not enabled
      // Ask user to enable GPS/network in settings
      gps.showSettingsAlert();
    }

    // get current date & time in Millis since Epoch
    Calendar rightNow = Calendar.getInstance();
    timestamp = rightNow.getTimeInMillis();
    String currentDateTimeString = DateFormat.getDateTimeInstance()
        .format(new Date());
    textViewNewLatitude = (TextView) findViewById(R.id.textViewNewLatitude);
    textViewNewLongitude = (TextView) findViewById(R.id.textViewNewLongitude);
    textViewNewTimestamp = (TextView) findViewById(R.id.textViewNewTimestamp);
    editTextNewDescription = (EditText) findViewById(R.id.editTextNewDescription);
    editTextNewContent = (EditText) findViewById(R.id.editTextNewContent);
    buttonSaveNewGpsLocation = (Button) findViewById(R.id.buttonSaveNewGpsLocation);
    // display latitude and longitude
    textViewNewLatitude.setText(new Double(latitude).toString());
    textViewNewLongitude.setText(new Double(longitude).toString());
    // textView is the TextView view that should display it
    textViewNewTimestamp.setText(currentDateTimeString);
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.new_gpslocation, menu);
    return true;
  }
  public void onTakePhotoClicked(View v)
  {
    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    if (takePictureIntent.resolveActivity(getPackageManager()) != null)
    {
        startActivityForResult(takePictureIntent, 1);
    }
  }
  ImageView mImageView;
  Bundle bndl;
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
	  mImageView = (ImageView)findViewById(R.id.imageView1);
      if (requestCode == 1 && resultCode == RESULT_OK)
      {
          Bundle extras = data.getExtras();
          Bitmap imageBitmap = (Bitmap) extras.get("data");
          mImageView.setImageBitmap(imageBitmap);
          bndl = extras;
      }
  }
  public void onSavePicClicked(View v) throws IOException
  {
	  savePicClicked = true;
  }
  boolean savePicClicked = false;
  public void clickButtonSaveNewGpsLocation(View v)
  {
    Toast.makeText(getApplicationContext(), "Saving New GPS Location ",
                   Toast.LENGTH_SHORT).show();
    GpsLocation gpsLocation = new GpsLocation();
    gpsLocation.latitude = latitude;
    gpsLocation.longitude = longitude;
    gpsLocation.timestamp = timestamp;

    gpsLocation.description = editTextNewDescription.getText().toString();
    gpsLocation.content = editTextNewContent.getText().toString();
    gpsLocation.selected = MainActivity.YES;
    gpsLocation.save();
    
    if(savePicClicked)
    {
  	  //Date date = new Date();
  	  //String str = "mapimg_"+
      		  //new Long(date.getTime()).toString()+".png";
  	  
  	  Bitmap bmp = (Bitmap)bndl.get("data");
  	  
  	FileOutputStream out = null;
  	File output = null;
  	try {
  		output = createImageFile();
  	       out = new FileOutputStream(output); //str);
  	       bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
  	} catch (Exception e) {
  	    e.printStackTrace();
  	} finally {
  	       try{
  	           out.close();
  	       } catch(Throwable ignore) {}
  	}
        Picture pic = new Picture(output.getAbsolutePath(), gpsLocation.getId()); // str);
        pic.save();
        // ------------
        Toast.makeText(this, "Pic saved", Toast.LENGTH_SHORT).show();
    }

    finish();
  }
  String mCurrentPhotoPath;

  private File createImageFile() throws IOException {
      // Create an image file name
      String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
      String imageFileName = "JPEG_" + timeStamp + "_";
      File storageDir = Environment.getExternalStoragePublicDirectory(
              Environment.DIRECTORY_PICTURES);
      File image = File.createTempFile(
          imageFileName,  /* prefix */
          ".jpg",         /* suffix */
          storageDir      /* directory */
      );

      // Save a file: path for use with ACTION_VIEW intents
      mCurrentPhotoPath = "file:" + image.getAbsolutePath();
      return image;
  }
}
