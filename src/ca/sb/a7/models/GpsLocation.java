package ca.sb.a7.models;



import com.activeandroid.*;
import com.activeandroid.query.*;
import com.activeandroid.annotation.*;

import java.util.*;

import android.content.*;

@Table(name = "gpslocations")
public class GpsLocation extends Model 
{
  @Column(name = "latitude")
  public double latitude;

  @Column(name = "longitude")
  public double longitude;

  @Column(name = "description")
  public String description;

  @Column(name = "content")
  public String content;

  @Column(name = "timestamp")
  public long timestamp;
  
  @Column(name = "selected")
  public int selected;

  public GpsLocation()
  {
    super();
  }

  public GpsLocation(double latitude, double longitude, String description, String content, long timestamp, int selected)
  {
    super();
    this.latitude = latitude;
    this.longitude = longitude;
    this.description = description;
    this.content = content;
    this.timestamp = timestamp;
    this.selected = selected;
  }

  public static List<GpsLocation> getAll()
  {
    return new Select().all().from(GpsLocation.class) .execute();
  }
  

  // Examples of queries
  /********
  public static Item getRandom1()
  {
    return new Select().from(Item.class).orderBy("RANDOM()").executeSingle();
  }

  public static Item getRandom2(Category category)
  {
    return new Select().from(Item.class)
        .where("Category = ?", category.getId()).orderBy("RANDOM()")
        .executeSingle();
  }

  public static List<Item> getAll(Context context, Category category)
  {
    return new Select().from(Item.class)
        .where("Category = ?", category.getId()).orderBy("Name ASC").execute();
  }
  *******/


}

