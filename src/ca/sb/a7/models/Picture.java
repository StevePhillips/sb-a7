package ca.sb.a7.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Pictures")
public class Picture extends Model
{
	@Column(name = "Path")
	public String path;
	@Column(name = "Location")
	public long location;
	
	public Picture()
	{
		super();
	}
	public Picture(String pic, long loc)
	{
		super();
		path = pic;
		location = loc;
	}
}
