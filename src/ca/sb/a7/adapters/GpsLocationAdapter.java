package ca.sb.a7.adapters;

import java.util.List;

import ca.sb.a7.R;

import ca.sb.a7.*;
import ca.sb.a7.models.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;


public class GpsLocationAdapter extends ArrayAdapter<GpsLocation>
{
  private final List<GpsLocation> list;
  private final Activity context;

  public GpsLocationAdapter(Activity context, List<GpsLocation> list)
  {
    //super(context, com.fn.rest1.R.layout.list_item, list);
    super(context, R.layout.gps_location_single_list_item, list);
    this.context = context;
    this.list = list;
  }
  public int getCount()
  {
    return list.size();
  }
  @Override
  public void clear()
  {
    list.clear();
  }
  @Override
  public void add(GpsLocation gpsLocation )
  {
    list.add(gpsLocation);
  }
  @Override
  public GpsLocation getItem(int position)
  {
    return list.get(position);
  }
  @Override
  public long getItemId(int position)
  {
    return position;
  }
  static class ViewHolder
  {
    public TextView textViewGpsLocationId;
    public TextView textViewGpsLocationDescription;
    public Button buttonDeselectGpsLocation;
    public Button buttonUpdateGpsLocation;
    public Button buttonDeleteGpsLocation;
  }
  @Override
  public View getView(final int position, View convertView, ViewGroup parent)
  {
    final ViewHolder holder;

    if (convertView == null)
    {
      LayoutInflater inflator = context.getLayoutInflater();
      convertView = inflator
          .inflate(ca.sb.a7.R.layout.gps_location_single_list_item, null);

      holder = new ViewHolder();
      holder.textViewGpsLocationId = (TextView) convertView.findViewById(R.id.textViewGpsLocationId);
      holder.textViewGpsLocationDescription = (TextView) convertView
          .findViewById(R.id.textViewGpsLocationDescription);
      holder.buttonDeselectGpsLocation = (Button) convertView
          .findViewById(R.id.buttonDeselectGpsLocation);
      holder.buttonUpdateGpsLocation = (Button) convertView
          .findViewById(R.id.buttonUpdateGpsLocation);
      holder.buttonDeleteGpsLocation = (Button) convertView
          .findViewById(R.id.buttonDeleteGpsLocation);

      convertView.setTag(holder);
    }
    else
    {
      holder = (ViewHolder) convertView.getTag();
    }
    final GpsLocation gpsLocation = list.get(position);
    holder.textViewGpsLocationId.setText("" + list.get(position).getId());
    holder.textViewGpsLocationId.setTag(gpsLocation);
    holder.textViewGpsLocationDescription.setText(list.get(position).description);
    holder.buttonDeselectGpsLocation.setText(
    list.get(position).selected == MainActivity.YES? "Disable":"Enable");

    holder.buttonDeselectGpsLocation.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        LinearLayout ll = (LinearLayout) v.getParent();
        TextView tv = (TextView) ll.getChildAt(0);
        GpsLocation gpsLocation = (GpsLocation) tv.getTag();
        // TO DO - temporarily: just use a Toast to see if the concept works
        Toast.makeText(context,
                       "ID = " + gpsLocation.getId() + "  "
                           + holder.textViewGpsLocationDescription.getText().toString(),
                       Toast.LENGTH_LONG).show();

        if(gpsLocation.selected == MainActivity.NO)
        {
        	gpsLocation.selected = MainActivity.YES;
        	holder.buttonDeselectGpsLocation.setText("Disable");
        }
        else
        {
        	gpsLocation.selected = MainActivity.NO;
        	holder.buttonDeselectGpsLocation.setText("Enable");
        }
        gpsLocation.save();
          /*
          GpsLocation deselectGpsLocation = new Select().from(GpsLocation.class).where("Id = ? ", gpsLocation.getId()).executeSingle();
          deselectGpsLocation.selected = MainActivity.NO;
          deselectGpsLocation.save();
          */
      }
    });
    holder.buttonUpdateGpsLocation.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        LinearLayout ll = (LinearLayout) v.getParent();
        TextView tv = (TextView) ll.getChildAt(0);
        GpsLocation gpsLocation = (GpsLocation) tv.getTag();
        Toast.makeText(context,
                       "Description = " + gpsLocation.getId() + "  "
                           + holder.textViewGpsLocationDescription.getText().toString(),
                       Toast.LENGTH_LONG).show();

        // Launching new Activity on selecting single List Item
        Intent updateIntent = new Intent(context, UpdateGpsLocationActivity.class);
        // sending data to new activity
        Bundle bundle = new Bundle();
        bundle.putInt("gpsLocationId", gpsLocation.getId().intValue());
        updateIntent.putExtras(bundle);

        context.startActivity(updateIntent);
      }
    });
    holder.buttonDeleteGpsLocation.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        LinearLayout ll = (LinearLayout) v.getParent();
        TextView tv = (TextView) ll.getChildAt(0);
        GpsLocation gpsLocation = (GpsLocation) tv.getTag();
        gpsLocation.delete();

        list.remove(position);
        notifyDataSetChanged();
      }
    });
    return convertView;
  }
}