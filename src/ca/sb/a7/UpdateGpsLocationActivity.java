package ca.sb.a7;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import com.activeandroid.query.Select;
import ca.sb.a7.R;
import ca.sb.a7.MainActivity;
import ca.sb.a7.models.GpsLocation;
import ca.sb.a7.models.Picture;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
public class UpdateGpsLocationActivity extends Activity
{
  double latitude;
  double longitude;
  long timestamp;
  int gpsLocationId;
  GpsLocation gpsLocation = null;
  TextView textViewGpsLocationLatitude;
  TextView textViewGpsLocationLongitude;
  TextView textViewGpsLocationTimestamp;
  EditText editTextGpsLocationDescription;
  EditText editTextGpsLocationContent;
  CheckBox checkBoxGpsLocationSelected;
  Button buttonSaveGpsLocationUpdate;
  ImageView imageView;
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_update_gps_location);
    Bundle bundle = getIntent().getExtras();
    gpsLocationId = bundle.getInt("gpsLocationId");
    gpsLocation = new Select().from(GpsLocation.class).where("Id = ?", gpsLocationId).executeSingle();
 // get current date & time in Millis since Epoch
    Calendar rightNow = Calendar.getInstance();
    timestamp = rightNow.getTimeInMillis();
    String currentDateTimeString = DateFormat.getDateTimeInstance()
        .format(new Date());
    textViewGpsLocationLatitude= (TextView)
    		findViewById(R.id.textViewGpsLocationLatitude);
    textViewGpsLocationLongitude= (TextView)
    		findViewById(R.id.textViewGpsLocationLongitude);
    textViewGpsLocationTimestamp= (TextView)
    		findViewById(R.id.textViewGpsLocationTimestamp);
    editTextGpsLocationDescription= (EditText)
    		findViewById(R.id.editTextGpsLocationDescription);
    editTextGpsLocationContent= (EditText)
    		findViewById(R.id.editTextGpsLocationContent);
    checkBoxGpsLocationSelected= (CheckBox)
    		findViewById(R.id.checkBoxGpsLocationSelected);
    buttonSaveGpsLocationUpdate= (Button)
    		findViewById(R.id.buttonSaveGpsLocationUpdate);
    // display latitude and longitude
    textViewGpsLocationLatitude.setText(new Double(gpsLocation.latitude).toString());
    textViewGpsLocationLongitude.setText(new Double(gpsLocation.longitude).toString());
    // textView is the TextView view that should display it
    textViewGpsLocationTimestamp.setText(currentDateTimeString);
    editTextGpsLocationDescription.setText(gpsLocation.description);
    editTextGpsLocationContent.setText(gpsLocation.content);
    checkBoxGpsLocationSelected.setChecked(gpsLocation.selected == MainActivity.YES);
    imageView = (ImageView)findViewById(R.id.imageView1);
    Picture pic = new Select().from(Picture.class).where("Location = ?", gpsLocation.getId()).executeSingle();
    if(pic != null)
    {
	    Bitmap bmp = BitmapFactory.decodeFile(pic.path);
	    imageView.setImageBitmap(bmp);
    }
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.update_gps_location, menu);
    return true;
  }
  public void clickButtonSaveGpsLocationUpdate(View v)
  {
    Toast.makeText(getApplicationContext(), "Saving Updated GPS Location ",
                   Toast.LENGTH_SHORT).show();

    gpsLocation.description = editTextGpsLocationDescription.getText().toString();
    gpsLocation.content = editTextGpsLocationContent.getText().toString();
    gpsLocation.selected = checkBoxGpsLocationSelected.isChecked() ? MainActivity.YES : MainActivity.NO;
    gpsLocation.save();

    finish();
  }
}
